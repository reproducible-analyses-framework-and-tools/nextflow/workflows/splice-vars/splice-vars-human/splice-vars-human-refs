import subprocess
import os

manual = []

# Set path to current directory
path = os.getcwd()
sums_path = path + '/human_sums.txt'


def check_prior(file):
    prior = str(subprocess.check_output([f'find . -name {file}'], shell=True))
    if file in prior:
        return True
    else:
        return False


def reference_check(i):
    with open(sums_path, 'r') as f:
        if i in f.read():
            print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
            return True
        else:
            print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
            return False


def download_reference(cmd, alt=None):
    if alt == None:
        file = cmd.rsplit('/', 1)[1]
    else:
        file = alt
    attempt = 0
#   while True:
#       if attempt == 3:
#           manual.append(f'{file}')
#           return False
    subprocess.run([f'{cmd}'], shell=True)
#       check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#       if reference_check(check):
    return True
#       else:
#           subprocess.run([f'rm {file}'], shell=True)
#           attempt += 1


### Genomic reference ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens'], shell=True)
subprocess.run([f'mkdir -p {path}/references/homo_sapiens/fasta'], shell=True)
os.chdir(f'{path}/references/homo_sapiens/fasta')

if not check_prior('Homo_sapiens_assembly38.fasta'):
    download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta')
    os.chdir(f'{path}/references/homo_sapiens')
else:
    print('Found pre-existing file: Homo_sapiens.assembly38.no_ebv.fa')

### GTF/GFF3 ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
os.chdir(f'{path}/references/homo_sapiens/annot')
if not check_prior('gencode.v37.annotation.gtf'):
    download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gtf.gz')
    subprocess.run(['gunzip gencode.v37.annotation.gtf.gz'], shell=True)
else:
    print('Found pre-existing file: gencode.v37.annotation.gtf')
os.chdir(f'{path}/references/homo_sapiens')

os.chdir(f'{path}/references/homo_sapiens/annot')
if not check_prior('gencode.v37.annotation.gff3'):
    download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_37/gencode.v37.annotation.gff3.gz')
    subprocess.run(['gunzip gencode.v37.annotation.gff3.gz'], shell=True)
else:
    print('Found pre-existing file: gencode.v37.annotation.gff3')
os.chdir(f'{path}/references/homo_sapiens')
